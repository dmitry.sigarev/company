const Company = (function(){
    let token = Symbol('token');
    let pass = Symbol('pass');
    let workers = Symbol('workers');


    const secretToken = 'secretToken';

    let id = new Date().valueOf();

    class Company{
        constructor(nameOfCompany, maxUsers){
            this.nameOfCompany = nameOfCompany;
            this.maxUsers = maxUsers;
            this.companyMembers = [];

        }

        static createSuperAdmin(company) {
            let superUser = new User("super", "user", true);
            superUser.fromCompany = company;
            company.companyMembers.push(superUser);
            return superUser;
        }

        get sizeOfWorkers(){
            return this[workers].length;
        }

        get currWorker(id){
            let findWorker = this[workers].find( person => person.id === id);
            return findWorker;
        }

    }


    class User {
       constructor(name, lastName){
           this.name = name;
           this.lastName = lastName;
           this.id = id;

           if (arguments[2] == true) {
            this.role = "Super User";
            this.token = "Secret Token";
           }
       }
    }
})